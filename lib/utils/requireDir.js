var fs = require('fs');
var path = require('path');
var _ = require('lodash');

module.exports = function requireDir(dir) {
  var files = fs.readdirSync(dir);
  if(!files) return console.error("error reading " + dir);
  var obj = {};
  _.each(files, function(file){
    if (_.toLower(file).search('README') !== -1 || file.charAt(0) === '.') return;
    var stat = fs.statSync(path.resolve(dir + "/"+ file));
    if (stat.isDirectory()) {
      return;
    } else {
      obj[file.replace('.js','')] = require(path.resolve(dir + "/"+ file));
    }
  });
  return obj;
};
