module.exports = {
  db: require('./db'),
  api: require('./api'),
  http: require('./http'),
  routes: require('./routes'),
  server: require('./server')
};
