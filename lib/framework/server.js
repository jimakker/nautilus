var api = require('./api');
var http = require('./http');

module.exports.init = function () {
  return api.init().then(function(API){
    return http.init(API);
  });
};
