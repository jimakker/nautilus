describe('Auth', function() {
  before(function(done) {
    APIPromise.then(function(a){
      agent = supertest.agent(a.app);
      a.API.models.user.destroy({where: {username: 'test'}})
      .then(function () {
        done();
      });
    });
  });

  describe('GET /api/auth/login', function() {
    it('should not get login information', function(done) {
      agent.get('/api/auth/login')
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(401);
        done(err);
      });
    });
  });

  describe('POST /api/auth/login', function() {
    it('should not login', function(done) {
      agent.post('/api/auth/login')
      .send({ username: 'test', password: 'test'})
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(401);
        done(err);
      });
    });
  });

  describe('POST /api/auth/register', function() {
    it('should register', function(done) {
      agent.post('/api/auth/register')
      .send({ username: 'test', password: 'test'})
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(200);
        done(err);
      });
    });
  });

  describe('POST /api/auth/login', function() {
    it('should login', function(done) {
      agent.post('/api/auth/login')
      .send({ username: 'test', password: 'test'})
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(200);
        done(err);
      });
    });
  });

  describe('GET /api/auth/login', function() {
    it('should get login information', function(done) {
      agent.get('/api/auth/login')
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(200);
        done(err);
      });
    });
  });

  describe('GET /api/auth/logout', function() {
    it('should logout', function(done) {
      agent.get('/api/auth/logout')
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(200);
        done(err);
      });
    });
  });

  describe('GET /api/auth/login', function() {
    it('should not get login information', function(done) {
      agent.get('/api/auth/login')
      // .expect(200)
      .end(function (err, res) {
        expect(res.status).to.eql(401);
        done(err);
      });
    });
  });

});
