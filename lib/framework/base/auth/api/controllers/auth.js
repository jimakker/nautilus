var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');

module.exports = {
  login: function (req, res) {
    var user = req.user.toJSON();
    this.API.models.user.findById(user.id, {
      include: {
        model: this.API.models.passport,
        where: {
          protocol: 'jwt',
          status: true
        },
        attributes: ['accessToken']
      }
    }).then(function (user) {
      user = user.toJSON();
      user.accessToken = user.passports[0].accessToken;
      delete user.passports;

      return res.json(user);
    });
  },

  post__register: function (req, res, next) {
    var user = req.body;
    var salt = bcrypt.genSaltSync(10);
    var API = this.API;
    user.passports = [{
      protocol: 'local',
      password: req.body.password
    }, {
      protocol: 'jwt',
      accessToken: jwt.sign({ accessTokenSecret: salt }, process.env.JWT_SECRET || 'secret'),
      accessTokenSecret: salt
    }];
    this.API.models.user.create(
      user,
      {
        include: [this.API.models.passport]
    }).then(function(user){
      API.models.user.findById(user.id, {
        include: {
          model: API.models.passport,
          where: {
            protocol: 'jwt',
            status: true
          },
          attributes: ['accessToken']
        }
      }).then(function (user) {
        user = user.toJSON();
        user.accessToken = user.passports[0].accessToken;
        delete user.passports;

        return res.json(user);
      });
    }, function (err) {
      return new API.errors.db(err);
    });
  },

  get__logout: function (req, res) {
    req.logout();
    res.json({ok: 'true'});
  },

  $get__login: function (req, res) {
    this.API.models.user.findById(req.user.id, {
      include: {
        model: this.API.models.passport,
        where: {
          protocol: 'jwt',
          status: true,
        },
        attributes: ['accessToken']
      }
    }).then(function (user) {
      user = user.toJSON();
      user.accessToken = user.passports[0].accessToken;
      delete user.passports;
      res.set('_csrf', res.locals._csrf);

      return res.json(user);
    });
  }
};
