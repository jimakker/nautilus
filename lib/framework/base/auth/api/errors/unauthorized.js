module.exports = function (req, res, next) {
  return res.status(401).json({
    message: 'unauthorized',
    errors: [{
      message: 'unauthorized request',
      type: 'auth'
    }]
  });
};
