module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      unique: true
    },
    email: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        User.hasMany(models.passport);
      }
    }
  });

  return User;
};
