var bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  var Passport = sequelize.define('passport', {
    protocol: DataTypes.STRING,
    password: DataTypes.STRING,
    accessToken: DataTypes.STRING,
    accessTokenSecret:DataTypes.STRING,
    provider: DataTypes.STRING,
    identifier: DataTypes.STRING,
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    instanceMethods: {
      validatePassword: function(password){
        return bcrypt.compareSync(password, this.password);
      }
    },
    hooks: {
      beforeCreate: function (passport, options) {
        if(passport.password) {
          passport.password = bcrypt.hashSync(passport.password, process.env.SALT_ROUNDS || 10);
        }
      },
      beforeUpdate: function (passport, options) {
        if(passport.password) {
          passport.password = bcrypt.hashSync(passport.password, process.env.SALT_ROUNDS || 10);
        }
      }
    },
    classMethods:{
      associate:function(models){
        Passport.belongsTo(models.user);
      }
    }
  });

  return Passport;
};
