var command = require(__dirname + '/../utils/command');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');

console.log('Enabling auth');

var PWD = process.env.PWD;

var files = [
  'api/controllers/auth.js',
  'api/models/user.js',
  'api/models/migrations/create-user.js',
  'api/models/passport.js',
  'api/models/migrations/create-passport.js',
  'api/errors/unauthorized.js',
  'api/middlewares/isLoggedIn.js',
  'api/services/auth.js',
  'test/controllers/auth.js'
];

var dependencies = [
  'bcrypt',
  'passport',
  'passport-local',
  'jsonwebtoken',
  'express-jwt',
  'lusca',
  'express-session'
];

var filesCheck = false;

_.each(files, function (file) {
  try {
    var stat = fs.statSync(path.resolve(PWD, file));
    if(stat) {
      filesCheck = true;
      console.log('File "' + file + '" exists in your project. Not going to overwrite it');
    }
  } catch (e) {
    // console.log(e);
  }
});

if (filesCheck) {
  console.log('You need to solve some issues. Exiting.');
  process.exit(1);
} else {
  _.each(files, function (file) {
    fs.writeFileSync(path.resolve(PWD, file), fs.readFileSync(path.resolve(PWD, 'lib/framework/base/auth/', file)));
  });
  console.log("Files copied, installing npm dependencies");
  command('npm',
    _.flatten(['install', '--save', dependencies]),
    {},
    function (err, result) {
      if(err) {
        console.log(err);
        process.exit(1);
      }
      console.log("Auth installed!");
    }
  );
}
