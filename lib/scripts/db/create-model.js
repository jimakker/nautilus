var command = require(__dirname + '/../../utils/command');

command('sequelize',
  ['model:create',
  '--name', process.argv[2],
  '--attributes', process.argv[3],
  '--models-path', 'api/models',
  '--migrations-path', 'api/models/migrations',
  '--seeders-path', 'api/models/seeders',
],
  {},
  function (err, result) {

  }
);
