var command = require(__dirname + '/../../utils/command');
var path = require('path');


var dbPath = path.resolve(process.argv[1], '../../../../db.sqlite');

command('sequelize',
  ['db:migrate',
  '--models-path', 'api/models',
  '--migrations-path', 'api/models/migrations',
  '--seeders-path', 'api/models/seeders',
  '--url', process.env.DB_URI || 'sqlite://' + dbPath
],
  {},
  function (err, result) {

  }
);
