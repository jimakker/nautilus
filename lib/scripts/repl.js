var repl = require('repl');
var api = require('../framework/api');
var vm = require('vm');
var lol = 'lol';
api.init().then(function(API){
  var r = repl.start({
    prompt: '>',
    eval: customEval,
    useColors: true
  });
  r.context.API = API;


  function customEval(cmd, context, filename, callback) {
    var result;
    try {
      result = vm.runInContext(cmd, context);
    } catch (e) {
      if (isRecoverableError(e)) {
        return callback(new repl.Recoverable(e));
      }
    }
    callback(null, result);
  }

  function isRecoverableError(error) {
    if (error.name === 'SyntaxError') {
      return /^(Unexpected end of input|Unexpected token)/.test(error.message);
    }
    return false;
  }
});
