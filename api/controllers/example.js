module.exports = {
  middlewares: [ 'exampleMiddleware',
    function (req, res, next) {
      this.API.logger.debug('adding middlewares in route object');
      next();
    }
  ],
  all: function (req, res) {
    res.json({all: 'overwriting "magic" function'});
  },
  get_post___hello__world: {
    middlewares: [ function (req, res, next) {
      this.API.logger.debug('adding middlewares in controller object');
      next();
    } ],
    controller: function (req, res) {
      res.json({hello:'world'});
    }
  },
  example: {
    controller: function (req, res) {
      this.API.logger.info("log from example with route and methods defined in object. Method: ", req.method);
      res.json({ hello: 'world'});
    },
    methods: ['get', 'post'],
    route: 'custom-route'
  }
};
