# nautilus

Fast prototyping, express and sequelize based, opinionated framework.

## Install

```bash
git clone https://gitlab.com/jimakker/nautilus.git
cd nautilus
npm install
```

## Run

```bash
npm run dev
```

## Dependencies

Nautilus uses these modules:

* bluebird
* body-parser
* cors
* express
* lodash
* morgan
* multer
* require-environment-variables
* sequelize
* sequelize-cli
* sqlite3
* winston

And these dev dependencies:

* mocha
* chai
* supertest

## API

Your application code lives in the `api/` directory. Logic is separated in *controllers*, *models*, *services*, *middlewares* and *errors*. Everything is initialized *automagically* into an object called **API**.

Everything (every function) in `api/controllers`, `api/errors`, `api/middlewares`, `api/services` gets its `this.API` binded to the apps **API** object, so *controllers*, *models*, *services*, *middlewares* and *errors* are accesible via `this.API`. File names (minus `.js`) are respected, so its case aware.

**API** gets some other properties:

### API._ (lodash)
You can use lodash as `this.API._ `.

### API.Promise (bluebird)
You can use bluebird as `this.API.Promise`.

### API.logger (winston)
`API.logger` is an winston logger (`debug`, `info`, `warning`, `error`). There are `API.logger.http` and `API.logger.sql` but are used internally to log http and sql logs.

See logger configuration in `lib/framework/logger.js`.

### `this.API` example

Here is an example of using `this.API` in a controller function:

```js
...
function (req, res) {
  this.API.models.mymodel
  .findAll()
  .then(function (things) {
    return res.json(things);
  }, function (err) {
    this.API.logger.error(err);
    return this.API.errors.myerror('A db error ocurred');
  });
}
...
```

### The `/api` directory

#### `api/models`
Sequelize model files. Everything you need to know is in [their documentation](http://docs.sequelizejs.com/en/latest/).

##### `api/models/migrations`
Sequelize migration files.

##### `api/models/seeders`
Sequelize seeder files.

#### `api/services`
Services contain logic that is not directly associated to a route controller, so they are reusable logic blocks with access to the **API** object.

Services can export an `init` function, which will be called on app init. Services can also have a `loadOrder` integer (used to launch services in order at startup) and `requiresApp` boolean, if requiresApp is `true`, service init function will be called with the express app as the argument and respecting `loadOrder` value.

#### `api/middlewares`
Reusable express middlewares. Example `api/middlewares/exampleMiddleware.js`:

```js
module.exports = function (req, res, next) {
  this.API.logger.debug('example middleware');
  next();
};
```
Middlewares can be used via `this.API.middlewares` but that's useful only in a bunch of cases. That's why middlewares can be *summoned* by their name in a route or controller definition via a `middlewares` property, wich is an `Array` containing names of middlewares living in the **API** object or middleware functions. These middlewares are added in order to the express app. See controller documentation for more.

#### `api/errors`
Reusable errors.

Example `api/error/example.js`
```js
module.exports = function (req, res, next, err) {
  return res.status(400).json({
    message: err.message
  });
};
```
`req`, `res` and `next` are automatically binded, so errors are called with only one argument:

```js
...
this.API.errors.example({
  message: 'some error'
});
...
```

#### `api/controllers`
Routes and controllers are defined here. Each file represents a route and it's controllers.

...

## Tests
Tests are located in the`test/` directory. Nautilus uses mocha, chai and supertest.

Run `npm run test`


## Extras
There are some extras bundled but not activated.

### Auth
Passport based auth system with local strategy, **bcrypt** is used to hash passwords. JWT integration, [lusca](https://github.com/krakenjs/lusca) for security (CSRF etc..).

#### Dependencies
* bcrypt
* passport
* passport-local
* jsonwebtoken
* express-jwt
* lusca
* express-session

#### Enable Auth
Just execute `npm run auth:enable`.

It will copy files to the `api/` and `test/` folders and `npm install --save` dependencies. Those files are located in `lib/framework/base/auth`.

Next time you start the app all should be loaded.
